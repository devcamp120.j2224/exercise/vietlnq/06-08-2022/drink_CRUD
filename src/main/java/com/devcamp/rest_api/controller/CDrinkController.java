package com.devcamp.rest_api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rest_api.model.CDrink;
import com.devcamp.rest_api.repository.IDrinkRepository;

@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository pDrinkRepository;

    @CrossOrigin
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){
        try {
            List<CDrink> vDrinks = new ArrayList<CDrink>();
            pDrinkRepository.findAll().forEach(vDrinks::add);

            return new ResponseEntity<>(vDrinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("/drinks")
    public ResponseEntity<CDrink> createDrink(@Valid @RequestBody CDrink pDrink) {
        try {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            CDrink _drinks = pDrinkRepository.save(pDrink);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
        try {
            Optional<CDrink> drinkData = pDrinkRepository.findById(id);
            if(drinkData.isPresent()){
                return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PutMapping("/drinks/{id}")
    public ResponseEntity<CDrink> updateDrink(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrink){
        try {
            Optional<CDrink> drinkData = pDrinkRepository.findById(id);
            if (drinkData.isPresent()) {
                CDrink drink = drinkData.get();
                drink.setMaNuocUong(pDrink.getMaNuocUong());
                drink.setTenNuocUong(pDrink.getTenNuocUong());
                drink.setDonGia(pDrink.getDonGia());
                drink.setGhiChu(pDrink.getGhiChu());
                drink.setNgayCapNhat(new Date());
                return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteDrinkById(@PathVariable("id") long id) {
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
